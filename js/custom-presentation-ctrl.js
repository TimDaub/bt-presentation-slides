(function(window) {

  var animationState = 0;

  document.onkeydown = function (e) {
    var prev, next;
    e = e || window.event;

    if(e instanceof Event) {
      prev = 65;
      next = 68;
    } else if(e instanceof KeyboardEvent) {  
      prev = 97;
      next = 100;
    }

    if(e.keyCode === prev) { // Back: A
      if(animationState > 0) {
        animationState -= 1;
        var img = document.querySelectorAll('.animate-img')[0];
        var url = img.src.split('-');
        var num = parseInt(url.pop().split('.')[0], 10)-1;
        var newSrc = url.join('-') + '-' + num + '.png';

        img.src = newSrc; 
      }
    } else if(e.keyCode === next) { // Forward: D
      if(animationState <= 24) {
        animationState += 1;
        var img = document.querySelectorAll('.animate-img')[0];
        var url = img.src.split('-');
        var num = parseInt(url.pop().split('.')[0], 10) + 1;
        var newSrc = url.join('-') + '-' + num + '.png';

        img.src = newSrc;
      }
    }
  };
  
})(window);